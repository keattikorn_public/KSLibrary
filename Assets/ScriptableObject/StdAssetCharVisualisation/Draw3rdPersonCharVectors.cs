﻿using KS.Character;
using UnityEngine;

namespace StdAssetCharVisualisation
{
    [RequireComponent(typeof(ThirdPersonCharacterKSModifiedForPhysics))]
    [RequireComponent(typeof(ThirdPersonUserControlKSModifiedForPhysics))]
    public class Draw3rdPersonCharVectors : MonoBehaviour
    {
        Transform theTransform;
        private float vectorInfoLenght = 1.5f;

        private ThirdPersonCharacterKSModifiedForPhysics thirdPersonChar;
        private ThirdPersonUserControlKSModifiedForPhysics thirdPersonCharUserControl;
    
        // Start is called before the first frame update
        void Start()
        {
            theTransform = GetComponent<Transform>();
            thirdPersonChar = GetComponent<ThirdPersonCharacterKSModifiedForPhysics>();
            thirdPersonCharUserControl = GetComponent<ThirdPersonUserControlKSModifiedForPhysics>();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.DrawLine(
                theTransform.position ,
                theTransform.position + (theTransform.forward)*vectorInfoLenght,
                Color.blue);
        
            Debug.DrawLine(
                theTransform.position ,
                theTransform.position + (theTransform.right)*vectorInfoLenght,
                Color.red);   
        
            Debug.DrawLine(
                theTransform.position ,
                theTransform.position + (theTransform.up)*vectorInfoLenght,
                Color.green);   
        
        
            Debug.DrawLine(
                theTransform.position ,
                theTransform.position + thirdPersonCharUserControl.MoveVector*thirdPersonChar.ForwardAmount,
                Color.yellow);   
        
        }
    }
}
