using UnityEngine;

namespace Naplab.KS.SOArchitecture
{
    [CreateAssetMenu]
    public class IntegerVariable : ScriptableObject
    {
        public int Value;
    }
}