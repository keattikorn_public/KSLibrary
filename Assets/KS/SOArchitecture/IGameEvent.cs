using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Naplab.KS.SOArchitecture
{
    public interface IGameEvent
    {
        void Raise();
        void RegisterListener(GameEventListener listener);
        void UnregisterListener(GameEventListener listener);
    }
}