using UnityEngine;

namespace Naplab.KS.SOArchitecture
{
    [CreateAssetMenu]
    public class StringVariable : ScriptableObject
    {
        public string Value;
    }
}