using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Naplab.KS.SOArchitecture
{
    [CreateAssetMenu]
    public class GameEvent : ScriptableObject
    {
        private List<GameEventListener> listenersList = new List<GameEventListener>();

        public void Raise()
        {
            foreach (var gel in listenersList)
            {
                gel.OnEventRaised();
            }
        }
        
        public void Raise(int value)
        {
            foreach (var gel in listenersList)
            {
                gel.OnEventRaised(value);
            }
        }       
 
        public void Raise(float value)
        {
            foreach (var gel in listenersList)
            {
                gel.OnEventRaised(value);
            }
        }  
        
        public void Raise(string value)
        {
            foreach (var gel in listenersList)
            {
                gel.OnEventRaised(value);
            }
        }  
        
        
        public void RegisterListener(GameEventListener listener)
        {
            listenersList.Add(listener);
        }

        public void UnregisterListener(GameEventListener listener)
        {
            listenersList.Remove(listener);
        }
    }
}

