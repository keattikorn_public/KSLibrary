using System;
using UnityEngine;

using UnityEngine.Events; //UnityEvent

namespace Naplab.KS.SOArchitecture
{
    public class GameEventListener : MonoBehaviour
    {
        public GameEvent gameEvent;
        
        public UnityEvent response;

        public UnityEvent<int> responseInt;

        public UnityEvent<float> responseFloat;

        public UnityEvent<string> responseString;

        
        public void OnEventRaised() => response.Invoke();
        public void OnEventRaised(int value) => responseInt.Invoke(value);
        public void OnEventRaised(float value) => responseFloat.Invoke(value);
        public void OnEventRaised(string value) => responseString.Invoke(value);
        
        private void OnEnable()
        {
            gameEvent.RegisterListener(this);
        }

        private void OnDisable()
        {
            gameEvent.UnregisterListener(this);
        }
    }
}
