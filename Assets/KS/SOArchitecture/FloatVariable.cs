using UnityEngine;

namespace Naplab.KS.SOArchitecture
{
    [CreateAssetMenu]
    public class FloatVariable : ScriptableObject
    {
        public float Value;
    }
}