using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Naplab.KS.SOArchitecture
{
    public interface IGameEventListener
    {
        void OnEventRaised();
    }
}