﻿using UnityEngine;

namespace KS.PhysicsLib
{
    public class DrawVectorGizmos : MonoBehaviour
    {
        [SerializeField]
        protected Vector3 startPosition;
        public Vector3 StartPosition{get;set;}

        [SerializeField]
        protected Vector3 direction;
        public Vector3 Direction { get; set; }

        [SerializeField]
        protected float length = 1.0f;
        public float Length { get; set; }

        [SerializeField]
        protected Color lineColor = Color.white;
        public Color LineColor { get; set; }

        protected float arrowHeadAngle = 20.0f;
        public float ArrowHeadAngle { get; set; }

        protected float arrowHeadLength = 0.25f;
        public float ArrowHeadLength { get; set; }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //Debug.DrawLine(_startPosition.position, _endPosition.position, _color);
        }

        private void OnDrawGizmos()
        {
            if (this.StartPosition != null && direction != null)
            {
                Vector3 endPosition = (this.StartPosition) + (direction.normalized * length);

                Gizmos.color = this.LineColor;
                Gizmos.DrawLine(this.StartPosition, endPosition);

                Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
                Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
                Gizmos.DrawRay(endPosition, right * arrowHeadLength);
                Gizmos.DrawRay(endPosition, left * arrowHeadLength);
            }
        }

    }
}