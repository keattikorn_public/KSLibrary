﻿using UnityEngine;

namespace KS.PhysicsLib
{
public class Utilities : MonoBehaviour {

    private static GameObject CreateGameObjectWithDrawVectorGizmosComponent(
        Vector3 startPosition, 
        Vector3 direction, 
        float magnitude, float showTime, Color color){

        GameObject go = new GameObject();
        go.name = "GizmosVector";
        DrawVectorGizmos dvg = go.AddComponent<DrawVectorGizmos>();
        dvg.StartPosition = startPosition;
        dvg.Direction = direction;
        dvg.LineColor = color;
        dvg.Length = magnitude;

        Destroy(go, showTime); 
        return go;
    }

    public static void CreateVectorGameObjectAt(
        Vector3 startPosition, 
        Vector3 direction, 
        float magnitude, float showTime)
    {
       CreateGameObjectWithDrawVectorGizmosComponent(startPosition,direction,magnitude,showTime,Color.red);
    }

    public static void CreateVectorGameObjectAt(
        Vector3 startPosition, 
        Vector3 direction, 
        float magnitude, float showTime,Color color)
    {
        CreateGameObjectWithDrawVectorGizmosComponent(startPosition,direction,magnitude,showTime,color);
    }
    /*
    public static Queue CopyQueue(Queue q) {
        Queue copy = new Queue();
        Queue copySecond = new Queue();

        while(q.Count > 0)        
        {
            object obj = q.Dequeue();
            copy.Enqueue(Utilities.Clone<object>(obj));
            copySecond.Enqueue(Utilities.Clone<object>(obj));
        }

        q = copySecond;
        return copy;
    }

    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }
    */
}
}