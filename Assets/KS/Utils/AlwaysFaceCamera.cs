﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AlwaysFaceCamera : MonoBehaviour
{
     public Transform tmProTransform;
    // Start is called before the first frame update
    void Start()
    {
        if (tmProTransform == null)
        {
            tmProTransform = GetComponent<Transform>();
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        if (tmProTransform == null) return;
        
        tmProTransform.rotation = Quaternion.LookRotation( tmProTransform.position - Camera.main.transform.position );
    }
}
