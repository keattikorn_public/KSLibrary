﻿using UnityEngine;

namespace KS.Utils
{
    public class CollisionEventsForwarder : MonoBehaviour
    {
        public delegate void EventHandler1<TParam>(TParam param);
    
    
        public event EventHandler1<Collision> OnCollisionEnterEvent = collision => {};
        public event EventHandler1<Collision> OnCollisionStayEvent = collision => {};
        public event EventHandler1<Collision> OnCollisionExitEvent = collision => {};
    
        private void OnCollisionEnter(Collision other)
        {
            OnCollisionEnterEvent(other);
        }

        private void OnCollisionStay(Collision other)
        {
            OnCollisionStayEvent(other);
        }

        private void OnCollisionExit(Collision other)
        {
            OnCollisionExitEvent(other);
        }
    }
}
