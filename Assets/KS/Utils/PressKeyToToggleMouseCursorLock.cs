﻿using UnityEngine;

namespace KS.Utils
{
    public class PressKeyToToggleMouseCursorLock : MonoBehaviour
    {
        [SerializeField] private KeyCode keycode = KeyCode.F12;


        [SerializeField] private bool isLock = false; 
        // Start is called before the first frame update
        private void Start()
        {
            ProcessLockingToggle();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(keycode))
            {
                isLock = !isLock;

                ProcessLockingToggle();
            }
        }

        private void ProcessLockingToggle()
        {
            // Lock or unlock the cursor.
            Cursor.lockState = isLock ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !isLock;
        }
    
        private void OnDisable()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
