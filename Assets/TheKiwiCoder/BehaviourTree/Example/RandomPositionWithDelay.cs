using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class RandomPositionWithDelay : ActionNode
{
    public Vector2 min = Vector2.one * -10;
    public Vector2 max = Vector2.one * 10;

    public float duration = 1;
    float startTime;
    
    protected override void OnStart() {
        startTime = Time.time;
    }

    protected override void OnStop() {
    }

    protected override State OnUpdate() {
        if (Time.time - startTime > duration) {
            blackboard.moveToPosition.x = Random.Range(min.x, max.x);
            blackboard.moveToPosition.z = Random.Range(min.y, max.y);
            
            return State.Success;
        }
        return State.Running;
    }
}
