using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class WaitRandomDuration : Wait
{
    public float min,max;

    protected override void OnStart()  {
        base.OnStart();

        duration = Random.Range(min,max);
    }

    protected override void OnStop() {

    }

    protected override State OnUpdate() {
        if (Time.time - startTime > duration)
        {
            return State.Success;
        }
        return State.Running;
    }
}
