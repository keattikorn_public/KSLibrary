using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

namespace KS.GameDevelopment4
{
    [CreateAssetMenu(fileName = "ItemSettings", menuName = "GameDev4/ItemSettings", order = 1)]
    public class ItemSettings : ScriptableObject
    {
        public int score;
        public float rotationAngularSpeed;
        public Color color;
    }
}