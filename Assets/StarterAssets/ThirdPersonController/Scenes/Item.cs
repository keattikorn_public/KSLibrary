using System;
using System.Collections;
using System.Collections.Generic;
using GameDevelopment4;
using UnityEngine;


namespace KS.GameDevelopment4{
    public class Item : MonoBehaviour
    {
        public ItemSettings itemSettings;
    
        // Start is called before the first frame update
        void Start()
        {
            this.gameObject.GetComponent<MeshRenderer>().material.color = itemSettings.color;
        }
    
        // Update is called once per frame
        void Update()
        {
            this.transform.RotateAround(
                gameObject.transform.position,
                Vector3.up,
                itemSettings.rotationAngularSpeed*Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            SingletonGameManager.Instance.score += itemSettings.score;
            Destroy(this.gameObject);
        }
    }
}
