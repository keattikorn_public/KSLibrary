using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Naplab.KS.GenericDesignPatterns;

namespace GameDevelopment4
{
    public class SingletonGameManager : Singleton<SingletonGameManager>
    {
        public int score;
    }
}