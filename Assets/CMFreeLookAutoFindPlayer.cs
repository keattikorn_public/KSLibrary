﻿using Cinemachine;
using UnityEngine;

namespace KS.StdAssetCharVisualisation
{
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class CMFreeLookAutoFindPlayer : MonoBehaviour
    {
        public CinemachineFreeLook FreeLookCam { get; set; }

        private Transform player;
    
        // Start is called before the first frame update
        void Start()
        {
            FreeLookCam = GetComponent<CinemachineFreeLook>();
        
            this.player = GameObject.FindWithTag("Player").transform;

            try
            {
                FreeLookCam.Follow = player;
                FreeLookCam.LookAt = player;
            
            }
            catch(UnityEngine.UnassignedReferenceException e)
            {
                FreeLookCam.Follow = player;
                FreeLookCam.LookAt = player;
            }



        }

    }
}
