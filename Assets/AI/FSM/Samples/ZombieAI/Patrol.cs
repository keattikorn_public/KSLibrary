﻿using UnityEngine;

namespace KS.AI.FSM.Samples.ZombieAI
{
    public class Patrol : State
    {
        public ZombieAIFSM localFSM { get; set; }
        
        private int currentWaypointIdx = -1;
        
        public Patrol(FiniteStateMachine fsm) : base(fsm)
        {
            localFSM = (ZombieAIFSM)fsm;

            localFSM.Agent.speed = 0.7f;
            
            //TODO: delete this line
            localFSM.CurrentStateName = this.GetType().Name;
        }

        public override void Enter()
        {
            float lastDist = Mathf.Infinity; // Store distance between NPC and waypoints.
            
            // Calculate closest waypoint by looping around each one and calculating the distance between the NPC and each waypoint.
            for (int i = 0; i < localFSM.wayPoints.Count; i++)
            {
                GameObject thisWP = localFSM.wayPoints[i].gameObject;
                float distance = Vector3.Distance(localFSM.NpcGameObject.transform.position, thisWP.transform.position);
                if(distance < lastDist)
                {
                    currentWaypointIdx = i;
                    lastDist = distance;

                    localFSM.AIChar.SetTarget(thisWP.transform);
                }            
            }
            
            localFSM.Anim.SetTrigger("Walk");
            
            //Proceed to the next stage of the FSM
            base.Enter();
        }
        
        public override void Update()
        {
            if (localFSM.AIChar.target != null)
                localFSM.Agent.SetDestination(localFSM.AIChar.target.position);

            if (localFSM.Agent.remainingDistance > localFSM.Agent.stoppingDistance)
            {
                //Move the agent
                localFSM.ThirdPersonChar.Move(localFSM.Agent.desiredVelocity, false, false);
            }
            else
            {
                //increase waypoint index
                if (currentWaypointIdx >= localFSM.wayPoints.Count-1)
                    currentWaypointIdx = 0;
                else
                    currentWaypointIdx++;

                //Set target to the next waypoint
                localFSM.AIChar.SetTarget(localFSM.wayPoints[currentWaypointIdx]);
                //localFSM.Agent.SetDestination(localFSM.AIChar.target.position);

                //Stop the character movement
                localFSM.ThirdPersonChar.Move(Vector3.zero, false, false);
            }
            
            
            
            ///
            if (localFSM.CanSeePlayer())
            {
                FSM.NextState = new CrawlChasing(FSM);
                this.StateStage = StateEvent.EXIT;
            }

        }
        
        public override void Exit()
        {
            
            localFSM.Anim.ResetTrigger("Walk");
            
            base.Exit();
        }
    }
}
