using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class RotateToFindTarget : ActionNode
{
    public string targetTag = "Player";

    public float detectDistance = 7;
    public float visAngle = 30;
    public float atkAngle = 5;
    
    public float idleRotSpeed = 30;
    public float atkRotSpeed = 60;
    
    protected override void OnStart() {
        
    }

    protected override void OnStop() {
        
    }

    protected override State OnUpdate() {
        
        return State.Success;
    }
}
