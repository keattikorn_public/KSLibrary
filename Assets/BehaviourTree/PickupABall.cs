using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class PickupABall : ActionNode
{
    protected override void OnStart() {
    }

    protected override void OnStop() {
    }

    protected override State OnUpdate()
    {
        TriggerInfo triggerInfo = context.gameObject.GetComponent<TriggerInfo>();

        if (triggerInfo == null) return State.Failure;

        foreach (Collider c in triggerInfo.listTriggedCollider)
        {
            if (c.gameObject.CompareTag("Ball"))
            {
                triggerInfo.listTriggedCollider.Remove(c);
                Destroy(c.gameObject);
                return State.Success;
            }
        }
        
        return State.Failure;
    }
}
