using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheKiwiCoder;

public class FindABall : ActionNode
{
    public float rotationSpeed = 60;
    private float rotateTime;
    private bool clockwise = false;
    
    protected override void OnStart()
    {
        RandomRotationParams();

    }

    protected override void OnStop() {
    }

    protected override State OnUpdate()
    {
        Vector3 rayStart = context.gameObject.transform.position;
        Vector3 rayDir = context.gameObject.transform.forward;
        RaycastHit hitInfo;
        if (Physics.Raycast(rayStart, rayDir,out hitInfo))
        {
            if (hitInfo.transform.gameObject.CompareTag("Ball"))
            {
                blackboard.targetPosition = hitInfo.transform.position;
                blackboard.moveToPosition = hitInfo.transform.position;
                return State.Success;
            }
        }
        
        context.gameObject.transform.Rotate(
            new Vector3(0,1,0),
            (clockwise==true ? 1 :-1 )*rotationSpeed*Time.deltaTime,
            Space.Self);

        rotateTime -= Time.deltaTime;
        if (rotateTime <= 0 )
        {
            return State.Failure;
        }
        
        
        return State.Running;
    }
    
    
    private void RandomRotationParams()
    {
        rotateTime = Random.Range(2,4);
        clockwise = Random.Range(0, 100) > 50 ? true : false;
    }
}
