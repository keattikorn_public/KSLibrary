using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInfo : MonoBehaviour
{
    public List<Collider> listTriggedCollider;

    private void Start()
    {
        listTriggedCollider = new List<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        listTriggedCollider.Add(other); 
    }

    private void OnTriggerStay(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        listTriggedCollider.Remove(other);
    }
}
